import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import com.google.gson.Gson;

public class RestApiToturial {

	public static void main(String[] args) throws Exception {

		Transcript transcript = new Transcript();
		transcript.setAudio_url("https://bit.ly/3yxKEIY");
		Gson gson = new Gson();
		String jsonReque = gson.toJson(transcript);
		
		System.out.println(jsonReque);

		HttpRequest postRequest = HttpRequest.newBuilder().uri(new URI("https://api.assemblyai.com/v2/transcript"))
				.header("Authorization", "88a3fd69ec2f4f17964bb48aa71b1f3e").POST(BodyPublishers.ofString(jsonReque))
				.build();

		HttpClient httpClient = HttpClient.newHttpClient();

		HttpResponse<String> postResponse = httpClient.send(postRequest, BodyHandlers.ofString());

		System.out.println(postResponse.body());

		transcript = gson.fromJson(postResponse.body(), Transcript.class);

		System.out.println(transcript.getId());

		HttpRequest getRequest = HttpRequest.newBuilder()
				.uri(new URI("https://api.assemblyai.com/v2/transcript/" + transcript.getId()))
				.header("Authorization", "88a3fd69ec2f4f17964bb48aa71b1f3e").build();

		while (true) {
			HttpResponse<String> getResponse = httpClient.send(getRequest, BodyHandlers.ofString());
			transcript = gson.fromJson(getResponse.body(), Transcript.class);

			System.out.println(transcript.getStatus());

			if ("completed".equals(transcript.getStatus()) || "error".equals(transcript.getStatus())) {
				break;
			}
			Thread.sleep(1000);
		}
		
		System.out.println("Transtription completed!");
		System.out.println(transcript.getText());
	}
}
